package debugger;

/**
 * Interface to be implemented by a program-like structure that can be debugged
 * by executing one step at a time, viewing the State and resetting with a new
 * konfiguration.
 */
public interface Debuggable {
    /**
     * Executes the next step.
     * 
     * @return true, if a step was executed.
     */
    boolean next();
    
    /**
     * @return a String representation of the whole state of this Debuggable.
     */
    String getStateAsString();
    
    /**
     * Resets the Debuggable with a String representation of a konfiguration.
     * (For example the tape of a turing machine or the counter values of a
     * counter programm.
     * 
     * @param konfiguration
     */
    void resetWith(String konfiguration);
}
