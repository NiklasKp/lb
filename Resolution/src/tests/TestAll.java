package tests;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import resolution.Clause;
import resolution.ClauseSetBuilder;
import resolution.ResolveAlgorithm;
import util.Classes;

/**
 * Tests all public static methods marked with the @ResolveAlgorithm Annotation
 * that are found in any classfile in the classpath
 */
@RunWith(Parameterized.class)
public class TestAll {
    
    private Set<Clause> createFirst() {
        return new ClauseSetBuilder()
            .add(-1, 2)
            .add(-2, 3)
            .add(1)
            .add(-3)
            .get();
    }
    
    private Set<Clause> createSecond() {
        return new ClauseSetBuilder()
            .add(-1, 2)
            .add(-2, 3)
            .add(1)
            .get();
    }
    
    private Set<Clause> createForN(int n) {
        ClauseSetBuilder builder = new ClauseSetBuilder();
        builder.add(1);
        for (int i = 1; i < n; i++) {
            builder.add(-i, (i + 1));
        }
        return builder.get();
    }
    
    @Test(timeout = 1000)
    public void testFirst() {
        assertThat(resolve(createFirst()), is(false));
    }
    
    @Test(timeout = 1000)
    public void testSecond() {
        assertThat(resolve(createSecond()), is(true));
    }
    
    @Test(timeout = 1000)
    public void testBig() {
        
        assertThat(resolve(createForN(8)), is(true));
    }
    
    @Test(timeout = 1000)
    public void testVeryBig() {
        assertThat(resolve(createForN(500)), is(true));
    }
    
    @Test(timeout = 1200)
    public void testGodzilla() {
        if(speed < ResolveAlgorithm.FAST) {
            fail("Didn't test");
        }
        assertThat(resolve(createForN(5000)), is(true));
    }
    
    // Parameterized stuff
    // ==================================================================
    
    @Parameter(value = 0)
    public Method resolveFunction;
    
    @Parameter(value = 1)
    public String methodName;

    @Parameter(value = 2)
    public int speed;
    
    @Parameters(name = "{1}, speed = {2}")
    public static Iterable<Object[]> getParameters() {
        List<Object[]> parameters = new ArrayList<>();
        
        for (Class<?> clazz : Classes.getAll()) {
            for (Method method : clazz.getDeclaredMethods()) {
                
                if (method.isAnnotationPresent(ResolveAlgorithm.class)
                    && Modifier.isStatic(method.getModifiers())
                    && Modifier.isPublic(method.getModifiers())
                    && method.getReturnType().equals(boolean.class)
                    && method.getParameterCount() == 1) {
                    
                    ResolveAlgorithm annotation = method.getAnnotation(ResolveAlgorithm.class);
                    
                    String name = annotation.name();
                    if (name.isEmpty()) {
                        name = method.getName();
                    }
                    
                    parameters.add(new Object[] {
                        method, name, annotation.speed()
                    });
                }
            }
        }
        // Sort by speed, descending
        Collections.sort(parameters, (o1, o2) -> Integer.compare((int) o2[2], (int) o1[2]));
        return parameters;
    }
    
    public boolean resolve(Set<Clause> toResolve) {
        try {
            return (boolean) resolveFunction.invoke(null, toResolve);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new RuntimeException(e.getCause());
        }
    }
}
