package resolution;

import java.util.HashSet;
import java.util.Set;

public class Clause {
    
    private Set<Integer> literals;
    
    public Clause() {
        this.literals = new HashSet<>();
    }
    
    public Clause(Set<Integer> literals) {
        this.literals = literals;
    }
    
    public Clause(int... literals) {
        this();
        for (int i : literals) {
            this.literals.add(i);
        }
    }
    
    public void addLiteral(int literal) {
        literals.add(literal);
    }
    
    public boolean contains(int literal) {
        return literals.contains(literal);
    }
    
    public Set<Integer> getLiterals() {
        return literals;
    }
    
    public Integer size(){
    	return literals.size();
    }
    
    public Clause resolveWith(Clause other, int literalToUse) {
        Clause result = null;
        if (other.contains(-literalToUse)) {
            result = new Clause();
            result.getLiterals().addAll(literals);
            result.getLiterals().addAll(other.getLiterals());
            result.getLiterals().remove(literalToUse);
            result.getLiterals().remove(-literalToUse);
        }
        return result;
    }
    
    public Set<Clause> resolveAllWith(Clause other) {
        Set<Clause> result = new HashSet<>();
        for (int literal : literals) {
            int negativeLiteral = -literal;
            if (other.contains(negativeLiteral)) {
                Clause resultClause = new Clause();
                Set<Integer> resultLiterals = resultClause.getLiterals();
                
                resultLiterals.addAll(literals);
                resultLiterals.addAll(other.getLiterals());
                
                resultLiterals.remove(literal);
                resultLiterals.remove(negativeLiteral);
                result.add(resultClause);
            }
        }
        return result;
    }
    
    /**
     * One step of resolving should check ALL complement literals, not just one.
     * Use <code>resolveAllWith</code> instead.
     */
    @Deprecated
    public Clause resolveFirstWith(Clause other) {
        for (int literal : literals) {
            int negativeLiteral = -literal;
            if (other.contains(negativeLiteral)) {
                Clause result = new Clause();
                Set<Integer> resultLiterals = result.getLiterals();
                
                resultLiterals.addAll(literals);
                resultLiterals.addAll(other.getLiterals());
                
                resultLiterals.remove(literal);
                resultLiterals.remove(negativeLiteral);
                return result;
            }
        }
        return null;
    }
    
    public boolean isBox() {
        return literals.size() == 0;
    }
    
    @Override
    public boolean equals(Object other){
        if(this == other){
            return true;
        }
        if(other instanceof Clause){
            return this.getLiterals().equals(((Clause)other).getLiterals());
        }
        return false;
    }
    
    @Override
    public int hashCode(){
        return this.getLiterals().hashCode();
    }
    
    @Override
    public String toString() {
        return literals.toString();
    }
}
