package resolution;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import static resolution.ResolveAlgorithm.*;

public class DefaultAlgorithms {
    @ResolveAlgorithm(name = "Naive and very stupid", speed = SLOWEST)
    public static boolean resolveNaive(Set<Clause> toResolve) {
        Set<Clause> newResolvents;
        do {
            newResolvents = new HashSet<>();
            for (Clause clause1 : toResolve) {
                for (Clause clause2 : toResolve) {
                    Set<Clause> res = clause1.resolveAllWith(clause2);
                    newResolvents.addAll(res);
                }
            }
            newResolvents.removeIf(c -> toResolve.contains(c));
            toResolve.addAll(newResolvents);
        } while (!newResolvents.isEmpty());
        
        for (Clause c : toResolve) {
            if (c.isBox()) {
                return false;
            }
        }
        return true;
    }
    
    @ResolveAlgorithm(name = "Default (with for-loops)", speed = SLOW)
    public static boolean resolveDefault(Set<Clause> toResolve) {
        Set<Clause> resolvents = toResolve;
        
        while (!resolvents.isEmpty()) {
            Set<Clause> newResolvents = new HashSet<>();
            for (Clause clause1 : resolvents) {
                for (Clause clause2 : toResolve) {
                    
                    Set<Clause> res = clause1.resolveAllWith(clause2);
                    for (Clause c : res) {
                        if (c.isBox()) {
                            return false;
                        }
                        newResolvents.add(c);
                    }
                }
            }
            resolvents = newResolvents;
        }
        return true;
    }
    
    @ResolveAlgorithm(name = "Default (small clauses first)", speed = MEDIUM)
    public static boolean resolveSorted(Set<Clause> toResolve) {
        Comparator<Clause> comparator = (o1, o2) -> Integer.compare(o1.getLiterals().size(),
            o2.getLiterals().size());
            
        SortedSet<Clause> resolvents = new TreeSet<>(comparator);
        resolvents.addAll(toResolve);
        
        while (!resolvents.isEmpty()) {
            SortedSet<Clause> newResolvents = new TreeSet<>(comparator);
            for (Clause clause1 : resolvents) {
                for (Clause clause2 : toResolve) {
                    
                    Set<Clause> resolvent = clause1.resolveAllWith(clause2);
                    for (Clause c : resolvent) {
                        if (c.isBox()) {
                            return false;
                        }
                        newResolvents.add(c);
                    }
                }
            }
            newResolvents.removeIf(e -> toResolve.contains(e));
            resolvents = newResolvents;
        }
        return true;
    }
}
