package resolution;

import static resolution.ResolveAlgorithm.FAST;

import java.util.Set;
import java.util.stream.Collectors;

public class MarkingAlgorithm {
    private static boolean first;
    private static boolean second;
    
    @ResolveAlgorithm(name = "Markierungalgorythmus1.0", speed = FAST)
    public static boolean resolveHF(Set<Clause> toResolve) {
        Set<Integer> marked = toResolve.stream()
            .filter(c -> c.getLiterals().size() == 1)
            .map(c2 -> c2.getLiterals().iterator().next())
            .collect(Collectors.toSet());
            
        while ((first = containsFirstForm(toResolve, marked))
            || (second = containsSecondForm(toResolve, marked))) {
            if (first) {
                return false;
            }
            if (second) {
                marked.add(getPositive(getFirstSecondForm(toResolve, marked)));
            }
        }
        return true;
    }
    
    /*
     * checks the formula for clauses of the first form, negativ literals only:
     * -x1 v -x2 v ... v -xn. x1 to xn are all supposed to be marked.
     */
    private static boolean containsFirstForm(Set<Clause> clauses, Set<Integer> marked) {
        for (Clause clause : clauses) {
            for (int i : clause.getLiterals()){
                if (i > 0) break;
                if (i < 0 && !marked.contains(i)) break;
                return true;                
            }
        }
        return false;
    }
    
    /*
     * checks the formula for clauses of the second form, negativ literals only,
     * except one positive: -x1 v -x2 v ... v -xn v y. x1 to xn are all supposed
     * to be marked. y is not marked yet.
     */
    private static boolean containsSecondForm(Set<Clause> clauses, Set<Integer> marked) {
        return !(getFirstSecondForm(clauses, marked).isBox());
    }
    
    /*
     * returns the first clause that is in the 2. form: -x1 v -x2 v ... v -xn v
     * y. x1 to xn are all supposed to be marked. y is unmarked.
     */
    private static Clause getFirstSecondForm(Set<Clause> clauses, Set<Integer> marked) {
        for (Clause clause : clauses){
            for (int i : clause.getLiterals()){
                if (i > 0 && marked.contains(i)) break;
                if (i < 0 && !marked.contains(i)) break;
                return clause;                
            }
        }
        return new Clause();
    }
    
    /*
     * returns the first positive element of a clause Since we expect
     * hornclauses, it is the only one aswell.
     */
    private static int getPositive(Clause clause) {
        for (int i : clause.getLiterals()) {
            if (i > 0) {
                return i;
            }
        }
        return 0;
    }
}
