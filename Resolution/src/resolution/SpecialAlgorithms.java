package resolution;

import static resolution.ResolveAlgorithm.FAST;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;

public class SpecialAlgorithms {
    @ResolveAlgorithm(name = "N resolution", speed = FAST)
    public static boolean resolveN(Set<Clause> clauses) {
        return resolve(c -> c.getLiterals().stream().allMatch(literal -> literal < 0), clauses);
    }
    
    @ResolveAlgorithm(name = "P resolution", speed = FAST)
    public static boolean resolveP(Set<Clause> clauses) {
        return resolve(c -> c.getLiterals().stream().allMatch(literal -> literal > 0), clauses);
    }
    
    @ResolveAlgorithm(name = "Einheitsresolution", speed = FAST)
    public static boolean resolve1(Set<Clause> clauses) {
        return resolve(c -> c.getLiterals().size() == 1, clauses);
    }
    
    private static boolean resolve(Predicate<Clause> filter, Set<Clause> toResolve) {
        Set<Clause> resolvents = toResolve;
        
        while (!resolvents.isEmpty()) {
            Set<Clause> newResolvents = new HashSet<>();
            for (Clause clause1 : resolvents) {
                if (filter.test(clause1)) {
                    for (Clause clause2 : toResolve) {
                        
                        Set<Clause> res = clause1.resolveAllWith(clause2);
                        for (Clause c : res) {
                            if (c.isBox()) {
                                return false;
                            }
                            newResolvents.add(c);
                        }
                    }
                }
                
            }
            resolvents = newResolvents;
            newResolvents.removeAll(toResolve);
        }
        return true;
    }
}
