package resolution;

import java.util.*;

public class ClauseSetBuilder {
    private Set<Clause> result;
    
    public ClauseSetBuilder() {
        result = new HashSet<>();
    }
    
    public ClauseSetBuilder add(int... literals) {
        result.add(new Clause(literals));
        return this;
    }
    
    public Set<Clause> get() {
        return result;
    }
}
