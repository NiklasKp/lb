package resolution;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.ElementType;

/**
 * Annotation to mark an algorithm that resolves a clause set.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ResolveAlgorithm {
    int FASTEST = 100;
    int FAST = 75;
    int MEDIUM = 50;
    int SLOW = 25;
    int SLOWEST = 1;
    
    String name() default "";
    int speed() default SLOWEST;
}
