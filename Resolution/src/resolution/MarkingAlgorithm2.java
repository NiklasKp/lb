package resolution;

/*
 * nach größe sortieren.
 * merken wo wieviel noch fehlt.
 * andere richtungen gucken.
 * Vorsortierte Liste
 * pointer?
 * 
 */
import static resolution.ResolveAlgorithm.FAST;

import java.util.Set;
import java.util.stream.Collectors;

public class MarkingAlgorithm2 {
    private static Form check;
    private static Clause second_form;
    
    private static enum Form {
        FIRST, SECOND, NONE
    };
    
    @ResolveAlgorithm(name = "MarkierungalgorythmusVersion2.0", speed = FAST)
    public static boolean resolveHF(Set<Clause> toResolve) {
        Set<Integer> marked = toResolve.stream()
            .filter(c -> c.getLiterals().size() == 1)
            .map(c2 -> c2.getLiterals().iterator().next())
            .collect(Collectors.toSet());
            
        while ((check = getForm(toResolve, marked)) != Form.NONE) {
            if (check == Form.FIRST){
                return false;
            }
            if (check == Form.SECOND){
                marked.add(getPositive(second_form));
                toResolve.remove(second_form);
            }
        }
        return true;
    }
    
    /*
     * return the first positive element of a clause Since we expect
     * hornclauses, it is the only one aswell.
     */
    private static int getPositive(Clause clause) {
        for (int i : clause.getLiterals()) {
            if (i > 0) {
                return i;
            }
        }
        return 0;
    }
    
    /*
     * returns an enum representing the form the formula contains
     */
    public static Form getForm(Set<Clause> clauses, Set<Integer> marked) {
        for (Clause clause : clauses) {
            Form check = getForm(clause, marked);
            if (check != Form.NONE)
                return check;
        }
        return Form.NONE;
    }
    
    /*
     * returns an enum representing the form one clause contains
     */
    public static Form getForm(Clause clause, Set<Integer> marked) {
        int counter = 0;
        for (int i : clause.getLiterals()) {
            if (i > 0 && marked.contains(i)) {
                return Form.NONE;
            } else if (i < 0 && !marked.contains(i)) {
                return Form.NONE;
            } else if (i < 0 && marked.contains(i)) {
                counter++;
            }
        }
        if (clause.size() == counter)
            return Form.FIRST;
        second_form = clause;
        return Form.SECOND;
    }
}
