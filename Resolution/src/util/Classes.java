package util;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Utility class to collect Class-Objects from the .class files in the file
 * system. For convenience, all Exceptions are covered in RuntimeExceptions, or
 * replaced by Optional return values.
 * 
 * @author Niklas Kopp
 */
public class Classes {
    /**
     * Returns an Optional containing a new instance of the assigned class using
     * the empty constructor or an empty Optional if an Exception is thrown
     * while doing so.
     * 
     * @param clazz
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> Optional<T> getInstance(Class<?> clazz) {
        T result;
        try {
            result = (T) clazz.getConstructor().newInstance();
            
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
            | InvocationTargetException | NoSuchMethodException | SecurityException
            | ClassCastException e) {
            result = null;
        }
        return Optional.ofNullable(result);
    }
    
    /**
     * Returns all classes that implement the assigned interface class.
     * 
     * @param interfaceClass
     * @return
     * @throws IOException
     */
    public static Collection<Class<?>> getImplementing(Class<?> interfaceClass) {            
        return getAll()
            .stream()
            .filter(clazz -> Arrays.asList(clazz.getInterfaces()).contains(interfaceClass))
            .collect(Collectors.toList());
    }
    
    /**
     * Returns all classes found as .class Files under the root directories
     * 
     * @return
     * @throws IOException
     */
    public static Collection<Class<?>> getAll() {
        List<Class<?>> classes = new ArrayList<>();
        
        ClassLoader loader = ClassLoader.getSystemClassLoader();
        if (loader == null) {
            throw new RuntimeException("Couldn't get class loader");
        }
        try {
            for (URL u : Collections.list(loader.getResources(""))) {
                addClassFiles(u, classes);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return classes;
    }
    
    // Adds all classes found at 'root' to the assigned Collection.
    private static void addClassFiles(URL root, Collection<Class<?>> classes) {
        Path rootPath = null;
        try {
            rootPath = Paths.get(root.toURI());
        } catch (URISyntaxException e1) {
            throw new RuntimeException("Error while loading the resource " + root);
        }
        
        for (Path classFile : getClassFiles(rootPath, new ArrayList<>())) {
            try {
                String fullClassName = rootPath.relativize(classFile)
                    .toString()
                    .replace(".class", "")
                    .replace(File.separatorChar, '.');
                    
                classes.add(Class.forName(fullClassName));
            } catch (ClassNotFoundException e) {
                // It's not there, so it's not added. Nothing to do here.
            }
        }
    }
    
    // Adds all files in the assigned directory (and all sub-directories) that
    // end with '.class' to the assigned Collection, then returns it.
    private static Collection<Path> getClassFiles(Path parentPath, Collection<Path> classFiles) {
        try {
            for (Path p : Files.newDirectoryStream(parentPath)) {
                if (p.getFileName().toString().endsWith(".class")) {
                    classFiles.add(p);
                } else if (Files.isDirectory(p)) {
                    getClassFiles(p, classFiles);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return classFiles;
    }
    
    private Classes() {}
}
