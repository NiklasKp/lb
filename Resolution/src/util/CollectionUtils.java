package util;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import javafx.util.Pair;
import java.util.function.BiFunction;

/**
 * Convenience functions for operations on collections (partially inspired by
 * Ruby)
 * 
 * @author Steffen Albrecht
 */

public class CollectionUtils {
    
    /**
     * Combines the values at corresponding indices of two lists into a single
     * stream of pairs. If one list is longer than the other, the excess values
     * are discarded.
     * 
     * @param list1
     *            The first input list
     * @param list2
     *            The second input list
     * @return A stream of pairs
     */
    
    public static <T, U> Stream<Pair<T, U>> zipStream(List<T> list1, List<U> list2) {
        int size = Math.min(list1.size(), list2.size());
        return IntStream.range(0, size).mapToObj(i -> new Pair<T, U>(list1.get(i), list2.get(i)));
    }
    
    /**
     * Combines the values at corresponding indices of two lists into a single
     * stream of pairs. If one list is longer than the other, an exception is
     * thrown.
     * 
     * @param list1
     *            The first input list
     * @param list2
     *            The second input list
     * @return A stream of pairs
     * @throws IllegalArgumentException
     *             if the lists are not of equal length.
     */
    
    public static <T, U> Stream<Pair<T, U>> safeZipStream(List<T> list1, List<U> list2) {
        if (list1.size() != list2.size())
            throw new IllegalArgumentException("Lists are not of equal length");
        return IntStream.range(0, list1.size())
            .mapToObj(i -> new Pair<T, U>(list1.get(i), list2.get(i)));
    }
    
    /**
     * Combines the values at corresponding indices in two lists into single
     * values using a function passed as argument and returns them as a new
     * list. If one list is longer than the other, the excess values are
     * discarded.
     * 
     * @param list1
     *            The first input list
     * @param list2
     *            The second input list
     * @param function
     *            A BiFunction to combine the values at the corresponding
     *            indices into one
     * @return A list with the results of calling the function on corresponding
     *         pairs
     */
    
    public static <T, U, V> List<V> zip(List<T> list1, List<U> list2,
        BiFunction<T, U, V> function) {
        return zipStream(list1, list2).map(pair -> function.apply(pair.getKey(), pair.getValue()))
            .collect(Collectors.toList());
    }
    
    /**
     * Combines the values at corresponding indices in two lists into single
     * values using a function passed as argument and returns them as a new
     * list. If one list is longer than the other, an exception is thrown.
     * 
     * @param list1
     *            The first input list
     * @param list2
     *            The second input list
     * @param function
     *            A BiFunction to combine the values at the corresponding
     *            indices into one
     * @return A list with the results of calling the function on corresponding
     *         pairs
     * @throws IllegalArgumentException
     *             if the lists are not of equal length.
     */
    
    public static <T, U, V> List<V> safeZip(List<T> list1, List<U> list2,
        BiFunction<T, U, V> function) {
        return safeZipStream(list1, list2)
            .map(pair -> function.apply(pair.getKey(), pair.getValue()))
            .collect(Collectors.toList());
    }
    
    /**
     * Creates the Cartesian product of two collections as a single stream of
     * pairs.
     * 
     * @param collection1
     *            The first input collection
     * @param collection2
     *            The second input collection
     * @return A stream of pairs
     */
    
    public static <T, U> Stream<Pair<T, U>> productStream(Collection<T> collection1,
        Collection<U> collection2) {
        return collection1.stream().flatMap(collection1Item -> collection2.stream()
            .map(collection2Item -> new Pair<T, U>(collection1Item, collection2Item)));
    }
    
    /**
     * Combines the values of the tuples of the Cartesian product of two
     * collections into single values using a function passed as argument and
     * returns them as a new list.
     * 
     * @param collection1
     *            The first input collection
     * @param collection2
     *            The second input collection
     * @param function
     *            A BiFunction to combine the values of each tuple into one
     * @return A list with the results of calling the function on the tuples
     */
    
    public static <T, U, V> List<V> productList(Collection<T> collection1,
        Collection<U> collection2,
        BiFunction<T, U, V> function) {
        return productStream(collection1, collection2)
            .map(pair -> function.apply(pair.getKey(), pair.getValue()))
            .collect(Collectors.toList());
    }
    
    /**
     * Combines the values of the tuples of the Cartesian product of two
     * collections into single values using a function passed as argument and
     * returns them as a new set.
     * 
     * @param collection1
     *            The first input collection
     * @param collection2
     *            The second input collection
     * @param function
     *            A BiFunction to combine the values of each tuple into one
     * @return A set with the results of calling the function on the tuples
     */
    
    public static <T, U, V> Set<V> productSet(Collection<T> collection1, Collection<U> collection2,
        BiFunction<T, U, V> function) {
        return productStream(collection1, collection2)
            .map(pair -> function.apply(pair.getKey(), pair.getValue()))
            .collect(Collectors.toSet());
    }
    
    /**
     * Returns the unique combinations of each element of one collection with
     * any element of another collection as a single stream of unordered pairs.
     * 
     * @param collection1
     *            The first input collection
     * @param collection2
     *            The second input collection
     * @return A stream of unique unordered pairs
     */
    
    public static <T, U> Stream<Pair<T, U>> combinationPairStream(Collection<T> collection1,
        Collection<U> collection2) {
        return collection1.stream().flatMap(collection1Item -> collection2.stream()
            .filter(collection2Item -> !collection2Item.equals(collection1Item))
            .map(collection2Item -> new unorderedPair<T, U>(collection1Item, collection2Item))
            .distinct());
    }
    
    /**
     * Combines the values of the unique unordered combinations of each element
     * of one collection with any element of another collection into single
     * values using a function passed as argument and returns them as a new
     * list.
     * 
     * @param collection1
     *            The first input collection
     * @param collection2
     *            The second input collection
     * @param function
     *            A BiFunction to combine the values of each unique unordered
     *            pair into one
     * @return A list with the results of calling the function on the pairs
     */
    
    public static <T, U, V> List<V> combinationPairList(Collection<T> collection1,
        Collection<U> collection2, BiFunction<T, U, V> function) {
        return combinationPairStream(collection1, collection2)
            .map(pair -> function.apply(pair.getKey(), pair.getValue()))
            .collect(Collectors.toList());
    }
    
    /**
     * Combines the values of the unique unordered combinations of each element
     * of one collection with any element of another collection into single
     * values using a function passed as argument and returns them as a new set.
     * 
     * @param collection1
     *            The first input collection
     * @param collection2
     *            The second input collection
     * @param function
     *            A BiFunction to combine the values of each unique unordered
     *            pair into one
     * @return A set with the results of calling the function on the pairs
     */
    
    public static <T, U, V> Set<V> combinationPairSet(Collection<T> collection1,
        Collection<U> collection2, BiFunction<T, U, V> function) {
        return combinationPairStream(collection1, collection2)
            .map(pair -> function.apply(pair.getKey(), pair.getValue()))
            .collect(Collectors.toSet());
    }
    
    /**
     * Inverts a map, turning keys into values and vice versa.
     * 
     * @param map
     *            A key-value map
     * @return A new map with keys and values inverted
     * @throws IllegalArgumentException
     *             if duplicate values are found in the original map.
     */
    
    public static <K, V> Map<V, K> invertMap(Map<K, V> map) {
        if (map.keySet().size() > map.values().stream().distinct().count()) {
            throw new IllegalArgumentException("Map contains duplicate values, cannot invert");
        }
        return map.entrySet()
            .stream()
            .collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
    }
    
    /**
     * Initializes a Map by pairing items from two ordered lists as keys and
     * values.
     * 
     * @param keys
     *            An ordered list containing the keys for the map
     * @param values
     *            An ordered list containing the values for the map
     * @return A Map with the keys and values paired in the original order
     * @throws IllegalArgumentException
     *             if the number of keys and values doesn't match.
     */
    
    public static <K, V> Map<K, V> bulkInitializeMap(List<K> keys, List<V> values) {
        return safeZipStream(keys, values).collect(Collectors.toMap(Pair::getKey, Pair::getValue));
    }
    
    /**
     * A class representing an unordered pair of objects
     * 
     * @author Steffen Albrecht
     */
    
    private static class unorderedPair<T, U> extends Pair<T, U> {
        
        private static final long serialVersionUID = 1L;
        public Object value1, value2;
        
        public unorderedPair(T value1, U value2) {
            super(value1, value2);
            this.value1 = value1;
            this.value2 = value2;
        }
        
        @Override
        public boolean equals(Object other) {
            return other instanceof unorderedPair<?, ?>
                && (value1.equals(((unorderedPair<?, ?>) other).value1)
                    && value2.equals(((unorderedPair<?, ?>) other).value2)
                    || value1.equals(((unorderedPair<?, ?>) other).value2)
                        && value2.equals(((unorderedPair<?, ?>) other).value1));
        }
        
        @Override
        public int hashCode() {
            return 17 * (value1.hashCode() + value2.hashCode());
        }
        
    }
}
