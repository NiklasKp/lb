package countermachine;

/**
 * Stores and executes a jump command for a counter machine.  
 *
 */
public class JumpCommand extends Command {
    
    int thenJump;
    int elseJump;
    
    public JumpCommand(int number, int register, int thenJump, int elseJump, String source) {
        super(number, register, source);        
        this.thenJump = thenJump;
        this.elseJump = elseJump;
    }
    
    /**
     * 
     */
    int execute(int[] registers) {
        nextInstruction = registers[register] == 0 ? thenJump : elseJump;
        return nextInstruction;
    }    
}
