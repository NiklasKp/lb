package countermachine;

import java.util.List;

/**
 * Simulates a simple counter machine with a four-command instruction set.
 * 
 * Syntax is defined as 
 * Jump command: if0 r2 then 04 else 01 -> if the value in
 * register 2 is zero, jump to instruction 04, else to instruction 01.
 * Increment command: r1++ -> increment the value in register 1. 
 * Decrement command: r2-- -> increment the value in register 2.
 * Halt command: halt -> halt the machine. 
 * 
 */
public class CounterMachine {
    
    int[] registers = new int[16];
    List<Command> commands;
    int nextInstruction = 0;
    
    public CounterMachine(String sourceFileName) {
        commands = Parser.parseSourceFile(sourceFileName);
    }
    
    void setRegister(int register, int value) {
        registers[register] = value;
    }
    
    void setRegisters(int[] values) {
        registers = values;
    }
    
    int getNextInstruction() {
        return nextInstruction;
    }
    
    void next() {
        Command nextCommand = commands.get(nextInstruction);
        nextInstruction = nextCommand.execute(registers);
    }
    
    void run() {
        while (nextInstruction != -1) {
            next();
        }
    }
    
    public String toString() {
        StringBuffer state = new StringBuffer();
        for (int i = 0; i < registers.length; i++) {
            state.append(String.format("r%d: %d ", i, registers[i]));
        }
        state.append('\n');
        if (nextInstruction != -1) {
            state.append(commands.get(nextInstruction));
        }
        return state.toString();
    }
}
