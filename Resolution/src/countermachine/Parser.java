package countermachine;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Parser for counting machine commands. Correct syntax is assumed. 
 *
 */
public class Parser {
    
    static final String jump = "(\\d{2})\\sif0\\sr(\\d)\\sthen\\s(\\d{2})\\selse\\s(\\d{2})";
    static final String increment = "(\\d{2})\\sr(\\d)\\+\\+";
    static final String decrement = "(\\d{2})\\sr(\\d)\\-\\-";
    static final String halt = "\\d{2}\\shalt";
    
    static final Pattern jumpPattern = Pattern.compile(jump);
    static final Pattern incrPattern = Pattern.compile(increment);
    static final Pattern decrPattern = Pattern.compile(decrement);
    static final Pattern haltPattern = Pattern.compile(halt);
    
    public static List<Command> parseSourceFile(String sourceFileName) {
        try (Stream<String> stream = Files.lines(Paths.get(sourceFileName))) {
            return stream.map(line -> parseLine(line)).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public static Command parseLine(String input) {
        Matcher jumpMatcher = jumpPattern.matcher(input);
        Matcher incrMatcher = incrPattern.matcher(input);
        Matcher decrMatcher = decrPattern.matcher(input);
        Matcher haltMatcher = haltPattern.matcher(input);
        
        if (jumpMatcher.find()) {
            return new JumpCommand(
                Integer.parseInt(jumpMatcher.group(1)),
                Integer.parseInt(jumpMatcher.group(2)),
                Integer.parseInt(jumpMatcher.group(3)),
                Integer.parseInt(jumpMatcher.group(4)),
                input);
        } else if (incrMatcher.find()) {
            return new IncrementCommand(
                Integer.parseInt(incrMatcher.group(1)),
                Integer.parseInt(incrMatcher.group(2)),
                input);
        } else if (decrMatcher.find()) {
            return new DecrementCommand(
                Integer.parseInt(decrMatcher.group(1)),
                Integer.parseInt(decrMatcher.group(2)),
                input);
        } else if (haltMatcher.find()) {
            return new HaltCommand(0,0, input);
        } else {
            return null; 
        }
    }
}
