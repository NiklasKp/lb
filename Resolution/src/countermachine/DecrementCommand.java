package countermachine;

/**
 * Stores and executes a decrement command for a counter machine.  
 *
 */
public class DecrementCommand extends Command {

    public DecrementCommand(int nextInstruction, int register, String source) {
        super(nextInstruction, register, source);
    }
    
    int execute(int[] registers) {
        registers[register]--;
        return nextInstruction + 1;
    }    
}
