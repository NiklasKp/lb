package countermachine;

/**
 * Stores and executes the halt command for a counter machine.  
 *
 */
public class HaltCommand extends Command {

    public HaltCommand(int nextInstruction, int register, String source) {
        super(nextInstruction, register, source);
    }
    
    int execute(int[] registers) {
        return -1;
    }
}
