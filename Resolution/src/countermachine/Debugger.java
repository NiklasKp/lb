package countermachine;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Load and execute a counter machine program and debug the output on the
 * screen, line by line.
 * 
 */
public class Debugger {
    
    static Scanner scan;
    static int counter = 0;
    
    static int[] initializeRegisters(Scanner scan) {
        System.out.println("Please set the register values (whitespace-separated list)");
        int[] registers = new int[16];
        if (scan.hasNextLine()) {
            String input = scan.nextLine();
            int i = 0;
            Pattern p = Pattern.compile("\\d+");
            Matcher m = p.matcher(input);
            while (m.find()) {
                registers[i++] = Integer.parseInt(m.group());
            }
        }
        return registers;
    }
    
    static void debug(CounterMachine com) {
        System.out.println(
            "Start the debugger: 'n' or RETURN for next step\n, 'r' to run the remaining program\n, or enter number of steps to execute");
        do {
            if (scan.hasNextLine()) {
                String input = scan.nextLine();
                if (input.matches("(?i)r(un)?")) {
                    while (com.getNextInstruction() != -1) {
                        step(com);
                    }
                } else if (input.matches("((?i)n(ext)?)?")) {
                    step(com);
                } else if (input.matches("\\d+")) {
                    int steps = Integer.parseInt(input);
                    for (int i = 0; i < steps; i++) {
                        while (com.getNextInstruction() != -1) {
                            step(com);
                        }
                    }
                } else {
                    System.out.println("command not recognized");
                }
            }
        } while (com.getNextInstruction() != -1);
        System.out.println("Machine has reached halt command");
        
    }
    
    private static void step(CounterMachine com) {
        counter++;
        System.out.println("Step #" + counter);
        System.out.println(com);
        com.next();
    }
    
    public static void main(String... args) {
        String sourceFileName = util.UserInteraction.getFilePath("source file",
            System.getProperty("user.dir"), true);
        CounterMachine com = new CounterMachine(sourceFileName);
        scan = new Scanner(System.in);
        com.setRegisters(initializeRegisters(scan));
        debug(com);
        scan.close();
    }
}
