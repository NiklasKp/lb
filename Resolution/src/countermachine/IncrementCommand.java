package countermachine;

/**
 * Stores and executes an increment command for a counter machine.  
 *
 */
public class IncrementCommand extends Command {

    public IncrementCommand(int nextInstruction, int register, String source) {
        super(nextInstruction, register, source);
    }
    
    int execute(int[] registers) {
        registers[register]++;
        return nextInstruction + 1;
    }
}
