package countermachine;

/**
 * Stores and executes a command for a counter machine. 
 *
 */
public abstract class Command {
    int register;
    int nextInstruction;
    String source;
    
    public Command(int number, int register, String source) {
        this.register = register;
        this.nextInstruction = number;
        this.source = source;
    }
    
    int execute(int[] registers) {
        return nextInstruction;
    }
    
    public String toString() {
        return source;
    }
    
    
}
