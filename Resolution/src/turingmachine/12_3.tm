// Turing machine to calculate f(a^n b^m) = b^m a^n 

// Initial state
q1

// Transitions (state1, state2, read, write, move)
q1,q2,a,A,R
q1,q3,b,B,R
q1,q11,#,#,H

q2,q2,a,a,R
q2,q3,b,b,R
q2,q4,#,$,L

q3,q3,b,b,R
q3,q4,#,$,L

q4,q5,a,$,R
q4,q6,b,$,R
q4,q9,A,#,R
q4,q10,B,#,R

q5,q5,b,b,R
q5,q5,a,a,R
q5,q5,$,$,R
q5,q7,#,a,L

q6,q6,b,b,R
q6,q6,a,a,R
q6,q6,$,$,R
q6,q7,#,b,L

q7,q7,a,a,L
q7,q7,b,b,L
q7,q8,$,$,L

q8,q8,$,$,L
q8,q4,a,a,H
q8,q4,b,b,H
q8,q9,A,#,R
q8,q10,B,#,R

q9,q9,b,b,R
q9,q9,a,a,R
q9,q9,$,#,R
q9,q11,#,a,L

q10,q10,b,b,R
q10,q10,a,a,R
q10,q10,$,#,R
q10,q11,#,b,L

q11,q11,b,b,L
q11,q11,a,a,L
q11,q12,#,#,R

// Accepting states, comma separated
[q12]