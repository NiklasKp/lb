package turingmachine;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import debugger.Debuggable;

/**
 * Executable representation of a TuringMachine.
 */
public class TuringMachine implements Debuggable {
    
    private static final char EMPTY = '#';
    
    private final State initialState;
    private State state;
    
    private List<Character> tape;
    private int headPosition = 0;
    private int offset = 0;
    
    /**
     * Creates a new TuringMachine with an empty tape and the assigned state
     * graph
     * 
     * @param stateGraph
     *            the initial State connected to all other states via
     *            Transitions.
     */
    public TuringMachine(State stateGraph) {
        
        this.initialState = stateGraph;
        this.state = initialState;
        this.tape = new ArrayList<>();
        checkTape();
    }
    
    /**
     * Loads the machine's state graph from a file.
     * 
     * @param filePath
     *            the path of the file
     * @return a machine with the state graph from the file and an empty tape
     * @throws IOException
     */
    public static TuringMachine loadFromFile(Path filePath) throws IOException {
        return Reader.readTuringMachine(filePath);
    }
    
    /**
     * Resets the machine to the initial state and sets a new tape.
     * 
     * @param newTape
     */
    @Override
    public void resetWith(String newTape) {
        headPosition = 0;
        offset = 0;
        state = initialState;
        tape.clear();
        newTape.chars().forEach(c -> tape.add((char) c));
        checkTape();
    }
    
    public State getState() {
        return state;
    }
    
    /**
     * Executes the next Transition if possible
     * 
     * @return true if a transition was performed.
     */
    @Override
    public boolean next() {
        if (getState().isAccepting()) {
            return false;
        }
        
        Transition next = null;
        for (Transition trans : getState()) {
            if (trans.getRead() == read()) {
                next = trans;
                break;
            }
        }
        
        if (next != null) {
            executeTransition(next);
            return true;
        } else {
            return false;
        }
    }
    
    private void executeTransition(Transition trans) {
        write(trans.getWrite());
        move(trans.getMove());
        state = trans.getNewState();
    }
    
    private char read() {
        return tape.get(headPosition + offset);
    }
    
    private void write(char toWrite) {
        tape.set(headPosition + offset, toWrite);
    }
    
    private void move(char m) {
        if (m == 'R') {
            headPosition++;
        } else if (m == 'L') {
            headPosition--;
        }
        checkTape();
    }
    
    private void checkTape() {
        while (headPosition + offset < 0) {
            tape.add(0, EMPTY);
            offset++;
        }
        while (headPosition + offset >= tape.size()) {
            tape.add(EMPTY);
        }
    }
    
    @Override
    public String getStateAsString() {
        return toString();
    }
    
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        
        for (int i = 0; i < tape.size(); i++) {
            char next = tape.get(i);
            if (i == headPosition + offset) {
                result.append("[" + getState() + "]");
            }
            result.append(next);
        }
        return result.toString();
    }
}
