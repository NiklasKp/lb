package turingmachine;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Builder class to simplify building a State graph for a TuringMachine.
 */
public class StateBuilder {
    private State initialState;
    private Map<String, State> states;
    
    public StateBuilder(String initialStateName) {
        states = new LinkedHashMap<>();
        initialState = getOrCreate(initialStateName);
    }
    
    public StateBuilder addTransition(String nameFrom, String nameTo,
        char read, char write, char move) {
        
        State from = getOrCreate(nameFrom);
        State to = getOrCreate(nameTo);
        from.add(new Transition(to, read, write, move));
        
        return this;
    }
    
    public StateBuilder setAcceptingStates(Iterable<String> stateNames) {
        for (String str : stateNames) {
            getOrCreate(str).setAccepting(true);
        }
        return this;
    }
    
    public State get() {
        return initialState;
    }
    
    private State getOrCreate(String name) {
        State state = states.get(name);
        if (state == null) {
            state = new State(name);
            states.put(name, state);
        }
        return state;
    }
}
