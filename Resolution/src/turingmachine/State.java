package turingmachine;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Node in the TuringMachine State graph.
 */
public class State implements Iterable<Transition> {
    
    private String name;
    private List<Transition> transitions;
    private boolean accepting = false;
    
    State(String stateName) {
        this.name = stateName;
        transitions = new ArrayList<>();
    }
    
    public String getName() {
        return name;
    }
    
    public boolean isAccepting() {
        return accepting;
    }
    
    void setAccepting(boolean accept) {
        accepting = accept;
    }
    
    void add(Transition trans) {
        transitions.add(trans);
    }
    
    List<Transition> getTransitions() {
        return transitions;
    }
    
    @Override
    public Iterator<Transition> iterator() {
        return transitions.iterator();
    }
    
    @Override
    public String toString() {
        return name;
    }
}
