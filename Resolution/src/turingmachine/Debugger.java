package turingmachine;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Console Debugger for Turingmachines.
 */
public class Debugger {
    public static void debug(Path machineFilePath, int sleepTime) throws IOException {
        debug(TuringMachine.loadFromFile(machineFilePath), sleepTime);
    }
    
    public static void debug(TuringMachine machine, int sleepTime) {
        @SuppressWarnings("resource")
        Scanner scan = new Scanner(System.in);
        String word;
        
        while (true) {
            System.out.println("Type in word:");
            machine.resetWith(word = scan.nextLine());
            System.out.println(machine);
            
            int step = 0;
            
            while (machine.next()) {
                System.out.println(++step + " " + machine);
                try {
                    Thread.sleep(sleepTime);
                } catch (InterruptedException e) {}
            }
            String not = (machine.getState().isAccepting()) ? " " : " not ";
            System.out.println("Word '" + word + "'" + not + "accepted\n");
        }
    }
    
    public static void main(String[] args) throws IOException, URISyntaxException {
        debug(Paths.get(Debugger.class.getResource("12_3.tm").toURI()), 200);
    }
}
