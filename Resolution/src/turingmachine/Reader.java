package turingmachine;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * File reader for TuringMachine.
 */
class Reader {
    private static StringTokenizer tok;
    
    static TuringMachine readTuringMachine(Path filePath) throws IOException {
        List<String> lines = Files.readAllLines(filePath);
        
        StateBuilder builder = null;
        
        for (String lineString : lines) {
            lineString = lineString.trim();
            if (!lineString.isEmpty() && !lineString.startsWith("//")) {
                if (builder == null) {
                    builder = new StateBuilder(lineString);
                } else if (lineString.startsWith("[")) {
                    builder.setAcceptingStates(readStates(lineString));
                } else {
                    tok = new StringTokenizer(lineString, ", ");
                    builder.addTransition(
                        tok.nextToken(),
                        tok.nextToken(),
                        tok.nextToken().charAt(0),
                        tok.nextToken().charAt(0),
                        tok.nextToken().charAt(0));
                }
            }
        }
        return new TuringMachine(builder.get());
    }
    
    private static List<String> readStates(String line) {
        List<String> states = new ArrayList<>();
        tok = new StringTokenizer(line, "[], ");
        while (tok.hasMoreTokens()) {
            states.add(tok.nextToken());
        }
        return states;
    }
}
