package turingmachine;

/**
 * Edge in the TuringMachine State graph.
 */
public class Transition {
    
    private final State newState;
    private final char read;
    private final char write;
    private final char move;
    
    Transition(State newState, char read, char write, char move) {
        this.newState = newState;
        this.read = read;
        this.write = write;
        this.move = move;
    }
    
    public State getNewState() {
        return newState;
    }
    
    public char getRead() {
        return read;
    }
    
    public char getWrite() {
        return write;
    }
    
    public char getMove() {
        return move;
    }
}
