// Turing machine to accept the language a^n b^n c^n

// Initial state
q0

// Transitions (state1, state2, read, write, move)
q0,q1,a,A,R
q0,q8,#,#,H

q1,q1,B,B,R
q1,q1,a,a,R
q1,q2,b,B,R

q2,q2,b,b,R
q2,q2,C,C,R
q2,q3,c,C,R

q3,q3,c,c,R
q3,q4,#,#,L

q4,q6,C,C,L
q4,q5,c,c,L

q5,q5,a,a,L
q5,q5,b,b,L
q5,q5,c,c,L
q5,q5,B,B,L
q5,q5,C,C,L
q5,q0,A,A,R

q6,q6,C,C,L
q6,q6,B,B,L
q6,q8,A,A,H

// Accepting states, comma separated
[q8]

